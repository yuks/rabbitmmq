# RabbitMQ Dockerfile #


### Installation ###
```
#!python
git clone https://bitbucket.org/yuksbg/rabbitmmq.git
```

```
#!python

docker build -t="yuks/rabbitmq"  rabbitmmq/

```


### Usage ###

```
#!python

docker run -d -p 5672:5672 -p 15672:15672 yuks/rabbitmq

```

### Access ###

You can access your managment console at http://[your-ip]:15672
