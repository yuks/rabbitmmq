#
# RabbitMQ Dockerfile
#

FROM ubuntu:14.04
MAINTAINER Yuks <yuksel.saliev@gmail.com>

ADD bin/rabbitmq /usr/local/bin/

RUN \
  apt-get update && \
  apt-get install -y rabbitmq-server && \
  rabbitmq-plugins enable rabbitmq_management && \
  echo "[{rabbit, [{loopback_users, []}]}]." > /etc/rabbitmq/rabbitmq.config && \
  chmod +x /usr/local/bin/rabbitmq

ENV RABBITMQ_LOG_BASE /data/log
ENV RABBITMQ_MNESIA_BASE /data/mnesia

VOLUME ["/data/log", "/data/mnesia"]

WORKDIR /data

CMD ["rabbitmq"]

EXPOSE 5672
EXPOSE 15672